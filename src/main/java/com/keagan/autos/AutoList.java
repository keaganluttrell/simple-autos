package com.keagan.autos;

import java.util.ArrayList;
import java.util.List;

public class AutoList {

    public AutoList() {}

    private List<Auto> autos;

    public AutoList(List<Auto> autos) {
        this.autos = autos;
    }

    public List<Auto> getAutos() {
        return autos;
    }

    public void setAutos(ArrayList<Auto> autos) {
        this.autos = autos;
    }

    public boolean isEmpty() {
        return autos.size() == 0;
    }
}

