package com.keagan.autos;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AutosApplicationTests {

    @Autowired
    TestRestTemplate restTemplate;
    @Autowired
    AutoRepository autoRepository;
    List<Auto> autos;
    private RestTemplate patchRestTemplate;

    @BeforeEach
    void setup() {
        patchRestTemplate = restTemplate.getRestTemplate();
        HttpClient httpClient = HttpClientBuilder.create().build();
        patchRestTemplate.setRequestFactory((new HttpComponentsClientHttpRequestFactory(httpClient)));

        autos = new ArrayList<>();
        String[] makes = {"ford", "chevy", "jeep", "dodge", "gmc"};
        String[] colors = {"black", "blue", "red", "green", "yellow"};
        for (int i = 0; i < 5; i++) {
            Auto newAuto = new Auto("xdb" + i, makes[i], "model" + i, 1990 + i, colors[i]);
            autos.add(newAuto);
        }
        autoRepository.saveAll(autos);
    }

    @AfterEach
    void teardown() {
        autoRepository.deleteAll();
    }

    @Test
    void getAllAutos_None_returnsAutoList() {
        String uri = "/api/autos";
        ResponseEntity<AutoList> response = restTemplate.getForEntity(uri, AutoList.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(autos.size(), response.getBody().getAutos().size());
    }

    @Test
    void getAllAutos_None_returnsNoContent() {
        autoRepository.deleteAll();
        String uri = "/api/autos";

        ResponseEntity<AutoList> response = restTemplate.getForEntity(uri, AutoList.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void getAllAutos_Make_returnsAutoList() {
        String testMake = "ford";
        String uri = "/api/autos?make=" + testMake;
        ResponseEntity<AutoList> response = restTemplate.getForEntity(uri, AutoList.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().getAutos().size());
    }

    @Test
    void getAllAutos_Color_returnsAutoList() {
        String testColor = "black";
        String uri = "/api/autos?color=" + testColor;
        ResponseEntity<AutoList> response = restTemplate.getForEntity(uri, AutoList.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().getAutos().size());
    }

    @Test
    void getAllAutos_MakeColor_returnsAutoList() {
        String testMake = "ford";
        String testColor = "black";
        String uri = String.format("/api/autos?make=%s&color=%s", testMake, testColor);
        ResponseEntity<AutoList> response = restTemplate.getForEntity(uri, AutoList.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().getAutos().size());
    }

    @Test
    void getAllAutos_Make_returnsNoContent() {
        String testMake = "toyota";
        String uri = "/api/autos?make=" + testMake;
        ResponseEntity<AutoList> response = restTemplate.getForEntity(uri, AutoList.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void getAllAutos_Color_returnsNoContent() {
        String testColor = "white";
        String uri = "/api/autos?make=" + testColor;
        ResponseEntity<AutoList> response = restTemplate.getForEntity(uri, AutoList.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void getAllAutos_MakeColor_returnsNoContent() {
        String testMake = "toyota";
        String testColor = "white";
        String uri = String.format("/api/autos?make=%s&color=%s", testMake, testColor);
        ResponseEntity<AutoList> response = restTemplate.getForEntity(uri, AutoList.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void addAuto_Auto_returnsNewAuto() throws JsonProcessingException {
        Auto newAuto = new Auto("vin999", "honda", "accord", 1998, "black");
        String uri = "/api/autos/";

        String body = toJSON(newAuto);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<?> request = new HttpEntity<>(body, headers);

        ResponseEntity<Auto> response = restTemplate.postForEntity(uri, request, Auto.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(newAuto.getVin(), response.getBody().getVin());
    }

    @Test
    void addAuto_Auto_BadRequest() {
        String uri = "/api/autos/";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<?> request = new HttpEntity<>("", headers);

        ResponseEntity<Auto> response = restTemplate.postForEntity(uri, request, Auto.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void getAuto_vin_returnsAuto() {
        Auto auto = autos.get(0);
        String uri = "/api/autos/" + auto.getVin();

        ResponseEntity<Auto> response = restTemplate.getForEntity(uri, Auto.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Auto actual = response.getBody();
        assertNotNull(actual);
        assertEquals(auto.getVin(), actual.getVin());
        assertEquals(auto.getMake(), actual.getMake());
        assertEquals(auto.getModel(), actual.getModel());
        assertEquals(auto.getYear(), actual.getYear());
        assertEquals(auto.getColor(), actual.getColor());
    }

    @Test
    void getAuto_vin_returnsNoContent() {
        String uri = "/api/autos/unFound";

        ResponseEntity<Auto> response = restTemplate.getForEntity(uri, Auto.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void updateAuto_VinRequest_returnsUpdatedAuto() throws JsonProcessingException {
        Auto auto = autos.get(0);
        String uri = "/api/autos/" + auto.getVin();
        String body = toJSON(new UpdateOwnerRequest("joe", "orange"));

        ResponseEntity<?> responseEntity = patchRestTemplate
                .exchange(uri, HttpMethod.PATCH, getPostReqHeaders(body), Auto.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Auto returnedAuto = (Auto) responseEntity.getBody();
        assertNotNull(returnedAuto);
        assertEquals("joe", returnedAuto.getOwner());
        assertEquals("orange", returnedAuto.getColor());
    }

    @Test
    void updateAuto_VinRequest_returnsNoContent() {
        Auto auto = autos.get(0);
        String uri = "/api/autos/" + auto.getVin();
        String body = "";

        ResponseEntity<?> responseEntity = patchRestTemplate
                .exchange(uri, HttpMethod.PATCH, getPostReqHeaders(body), Auto.class);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    void updateAuto_VinRequest_BadRequest() throws JsonProcessingException {

        String uri = "/api/autos/unFoundVin";
        String body = toJSON(new UpdateOwnerRequest("joe", "orange"));

        ResponseEntity<?> responseEntity = patchRestTemplate
                .exchange(uri, HttpMethod.PATCH, getPostReqHeaders(body), Auto.class);

        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }


    @Test
    void deleteAuto_vin_returnsAccepted() {
        Auto auto = autos.get(0);
        String uri = "/api/autos/" + auto.getVin();
        ResponseEntity<Auto> responseBefore = restTemplate.getForEntity(uri, Auto.class);
        assertEquals(HttpStatus.OK, responseBefore.getStatusCode());
        restTemplate.delete(uri);
        ResponseEntity<Auto> responseAfter = restTemplate.getForEntity(uri, Auto.class);
        assertEquals(HttpStatus.NO_CONTENT, responseAfter.getStatusCode());
    }

    @Test
    void deleteAuto_vin_returnsNoContent() {
        String uri = "/api/autos/unFound";
        restTemplate.delete(uri);
        ResponseEntity<AutoList> response = restTemplate.getForEntity("/api/autos/", AutoList.class);
        assertNotNull(response.getBody());
        assertEquals(autos.size(), response.getBody().getAutos().size());
    }


    @Test
    void contextLoads() {
    }

    private HttpEntity<?> getPostReqHeaders(String body) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        return new HttpEntity<>(body, headers);
    }

    private String toJSON(Object auto) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(auto);
    }
}
