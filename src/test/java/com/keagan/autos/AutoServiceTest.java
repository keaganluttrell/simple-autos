package com.keagan.autos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AutoServiceTest {

    AutoService autoService;
    ArrayList<Auto> autos;

    @Mock
    AutoRepository autoRepository;

    @BeforeEach
    void setup() {
        autoService = new AutoService(autoRepository);
        autos = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Auto auto = new Auto("vin" + i, "ford", "t", 1921, "black");
            autos.add(auto);
        }
    }

    @Test
    void getAllAutos_NoArgs_returnsAutoList() {
        when(autoRepository.findAll()).thenReturn(autos);
        AutoList autoList = autoService.getAllAutos();
        assertNotNull(autoList);
        assertFalse(autoList.isEmpty());
    }

    @Test
    void getAllAutos_NoArgs_returnsEmpty() {
        when(autoRepository.findAll()).thenReturn(new ArrayList());
        AutoList autoList = autoService.getAllAutos();
        assertNull(autoList);
    }

    @Test
    void getAllAutos_Make_returnsAutoList() {
        when(autoRepository.findAllByMakeAndColor(anyString(), anyString())).thenReturn(autos);
        AutoList autoList = autoService.getAllAutos("ford", "");
        assertNotNull(autoList);
        assertFalse(autoList.isEmpty());
    }

    @Test
    void getAllAutos_Color_returnsAutoList() {
        when(autoRepository.findAllByMakeAndColor(anyString(), anyString())).thenReturn(autos);
        AutoList autoList = autoService.getAllAutos("", "blue");
        assertNotNull(autoList);
        assertFalse(autoList.isEmpty());
    }

    @Test
    void getAllAutos_MakeColor_returnsAutoList() {
        when(autoRepository.findAllByMakeAndColor(anyString(), anyString())).thenReturn(autos);
        AutoList autoList = autoService.getAllAutos("ford", "blue");
        assertNotNull(autoList);
        assertFalse(autoList.isEmpty());
    }

    @Test
    void getAllAutos_MakeColor_returnsEmptyList() {
        when(autoRepository.findAllByMakeAndColor(anyString(), anyString())).thenReturn(new ArrayList<>());
        AutoList autoList = autoService.getAllAutos("ford", "");
        assertNull(autoList);
    }

    @Test
    void addAuto_Vin_returnAuto() {
        Auto auto = autos.get(0);
        when(autoRepository.save(any(Auto.class))).thenReturn(auto);
        Auto returnedAuto = autoService.addAuto(auto);

        assertNotNull(returnedAuto);
        assertEquals(auto.getMake(), returnedAuto.getMake());
    }

    @Test
    void getAutoByVin_Vin_returnAuto() {
        Auto auto = autos.get(0);
        when(autoRepository.findByVin(anyString())).thenReturn(auto);
        Auto actual = autoService.getAutoByVin(auto.getVin());

        assertNotNull(actual);
        assertEquals(auto.getMake(), actual.getMake());
    }

    @Test
    void getAutoByVin_Vin_returnNull() {
        when(autoRepository.findByVin(anyString())).thenReturn(null);
        Auto actual = autoService.getAutoByVin("unFoundVin");

        assertNull(actual);
    }

    @Test
    void updateAuto_Vin_returnUpdatedAuto() {
        Auto auto = autos.get(0);
        auto.setOwner("j");
        auto.setColor("red");

        when(autoRepository.findByVin(anyString())).thenReturn(auto);
        when(autoRepository.save(auto)).thenReturn(auto);

        Auto actual = autoService.updateAutoByVin("vin", "j", "red");

        assertEquals("j", actual.getOwner());
        assertEquals("red", actual.getColor());
    }

    @Test
    void updateAuto_Vin_returnNoContent() {
        when(autoRepository.findByVin(anyString())).thenReturn(null);

        Auto actual = autoService.updateAutoByVin("vin", "j", "red");

        assertNull(actual);
    }

    @Test
    void deleteAuto_Vin_void() {
        Auto auto = autos.get(0);

        when(autoRepository.findByVin(anyString())).thenReturn(auto);
        autoService.deleteAuto("vin");

        verify(autoRepository).delete(any(Auto.class));
    }

    @Test
    void deleteAuto_Vin_exception() {
        Auto auto = autos.get(0);

        when(autoRepository.findByVin(anyString())).thenReturn(null);
        assertThrows(AutoNotFoundException.class, () -> {
            autoService.deleteAuto("vin");
        });

    }
}
